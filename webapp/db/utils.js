require('rootpath')();
const util = require('util');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const { User, UserLikes } = require('models/user');

const saltRounds = 10;

function SQLconnect() {
    console.log("Connecting to mongo")
    mongoose.connect('mongodb://localhost/my_database');
}

//saves a user in db
async function saveRecord(username, password) {  
    //trim all fields for whitespaces except password
    username = username.trim();
    const hashedPassword = await bcrypt.hash(password, saltRounds);
    let p = new User({
        username: username, 
        password: hashedPassword
    });
    await p.save();
}

//gets one record based on a dictionary - helper method
async function getRecord(dict) {
    const user = await User.findOne(dict).exec();
    return user;
}

async function getRecordByUsernameAndPassword(username, password) {
    console.log("getRecordByUsernameAndPassword");
    let user = await getRecord({username: username});
    if (!user)
        return Promise.reject("User not valid");
    const match = await bcrypt.compare(password, user.password);
    if (!match)
        return Promise.reject("User not valid");
    return user;
}

async function updatePasswordBasedOnProps(props, newPassword) {
    const hashedPassword = await bcrypt.hash(newPassword, saltRounds);
    //do only one call to the DB
    return await User.update(props, { $set: { password: hashedPassword }});
}

async function likeUser(fromId, toId) {
    let uLikes = new UserLikes();
    try {
        let destinationUser = await getRecord({_id: toId});
        uLikes.toName = destinationUser.username;    
    }catch(err) {
        console.log("No username");
       throw "No username";
    }
    uLikes.fromId = fromId;
    uLikes.toId = toId;
    uLikes.value = 1;
    return await uLikes.save();
}

async function unlikeUser(fromId, toId) {
    //should be only one
    return await UserLikes.deleteOne({fromId: fromId, toId: toId});
}

async function getNumLikesforUser(id) {
    return await UserLikes.countDocuments({toId: id}).exec()
}

async function mostLiked() {
    return await UserLikes.aggregate(
        [
           { $group : {_id: "$toName", total: {$sum: "$value"}  }},
           { $sort : { total : -1 } }
        ]).exec();
}

module.exports = {
    SQLconnect,
    saveRecord,
    getRecord,
    likeUser,
    unlikeUser,
    mostLiked,
    getNumLikesforUser,
    updatePasswordBasedOnProps,
    getRecordByUsernameAndPassword
}