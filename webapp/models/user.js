const mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

//no need to include refIds, keep it simple and decoupled
const UserLikesSchema = new Schema({
    fromId: {type: String, required: true},
    toId: {type: String, required: true},
    toName: {type: String, required: true},
    value: { type: Number, min: 1, max: 1, default: 0 },
});

const UserSchema = new Schema({
    username:  { type: String, match: /^[a-z0-9]+$/, index: true, required: true, unique: true},
    password:  { type: String, required: true, minlength: 4 },
    createdAt: {
        type: Date, 
        default: Date.now
     },
});

UserLikesSchema.index({ fromId: 1, toId: 1 }, { unique: true });
UserLikesSchema.plugin(uniqueValidator, {
    message: 'Sorry, cannot like a user twice'
});
UserSchema.plugin(uniqueValidator);

const User = mongoose.model('User', UserSchema);
const UserLikes = mongoose.model('UserLikes', UserLikesSchema);

module.exports = {
    User,
    UserLikes
};




