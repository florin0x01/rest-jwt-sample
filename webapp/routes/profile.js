var express = require('express');
var router = express.Router();
const {authenticate} = require('middleware/jwt');
const asyncHandler = require('express-async-handler');
const {getRecord, updatePasswordBasedOnProps} = require('db/utils');

/* GET home page. */
router.get('/',asyncHandler(async (req, res, next) => {
    try {
        let user = await getRecord({_id: req.user.sub});
        if (!user)
            return res.status(400).json({ message: 'Invalid Profile' });
        const {password, ...noPassword} = user._doc;
        return res.json(noPassword);      
    }catch(err) {
        return res.status(400).json({ message: 'Invalid Profile' });
    }
}));

router.post('/update-password', asyncHandler(async (req, res, next) => {
    try {
        if (!req.body.password || !req.user.sub) {
            return res.status(400).json({message: 'Invalid password/profile'});
        }
        await updatePasswordBasedOnProps({_id: req.user.sub}, req.body.password);
        return res.json({message: 'ok'});
    }catch(err) {
        console.log(err);
        return res.status(400).json({ message: 'Invalid Profile' });
    }
}));

module.exports = router;
