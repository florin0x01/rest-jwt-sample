var express = require('express');
var router = express.Router();
const asyncHandler = require('express-async-handler');
const {mostLiked} = require('db/utils');

/* GET users listing. */
router.get('/', asyncHandler(async (req, res, next) => {
  try {
    let ml = await mostLiked();
    res.json(ml);
  }catch(err) {
     console.log(err);
     return res.status(400).json({message: err});
  }
}));

module.exports = router;
