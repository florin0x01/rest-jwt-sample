var express = require('express');
var router = express.Router();
const asyncHandler = require('express-async-handler');
const {getRecord, likeUser, unlikeUser, getNumLikesforUser} = require('db/utils');


/* GET users listing. */
router.get('/:id', asyncHandler(async (req, res, next) => {
  try {
      if (!req.params.id) {
        return res.status(400).json({message: 'Id required'});
      }
      let user = await getRecord({_id: req.params.id});
      let numLikes = await getNumLikesforUser(req.params.id);
      if (!numLikes) 
        numLikes = 0;
      return res.json({username: user.username, likes: numLikes })
  }catch(err) {
      if ('name' in err && err.name === 'CastError')
          return res.status(400).json({message: 'Invalid id'});
     return res.status(400).json({message: err});
  }
}));

router.post('/:id/like', asyncHandler(async (req, res, next) => {
    try {
       if (req.params.id.trim() === req.user.sub) {
         return res.status(400).json({message: 'Hey, I can\'t like myself :('})
       }
       await likeUser(req.user.sub, req.params.id);
      return res.json({status: 'ok'})
    }catch(err) {
      return res.status(400).json({message: err});
    }
}));

router.delete('/:id/unlike', asyncHandler(async (req, res, next) => {
    try {
      if (req.params.id.trim() === req.user.sub) {
        return res.status(400).json({message: 'Hey, I can\'t unlike myself :)'})
      }
      await unlikeUser(req.user.sub, req.params.id);
     return res.json({status: 'ok'})
   } catch(err) {
     return res.status(400).json({message: err});
  }
}));

module.exports = router;
