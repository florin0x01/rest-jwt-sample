var express = require('express');
var router = express.Router();
const {saveRecord} = require('db/utils');
const asyncHandler = require('express-async-handler');


/* GET home page. */
router.post('/', asyncHandler(async (req, res, next) => {
   try {
        let {username, password} = req.body;
        if (!username || !password)
            return res.status(400).json({message: "Invalid request"});
        if (password.length < 4) 
            return res.status(400).json({message: "Invalid request"});

        await saveRecord(username, password);
        res.json({'signup': 1});
   } catch(err) {
       console.log(typeof(err));
       console.log(JSON.stringify(err));
       console.log(err.message);
       return res.status(400).json({message: err.message})
   }
}));

module.exports = router;
