var express = require('express');
var router = express.Router();
const {authenticate} = require('middleware/jwt');

/* GET home page. */
router.post('/',authenticate);

module.exports = router;
