require('rootpath')();

var express = require('express');
var path = require('path');
var logger = require('morgan');
const {jwt} = require('middleware/jwt');
const errorHandler = require('middleware/errorhandler')
const bodyparser = require('body-parser')

const usersRouter = require('routes/users');
const signupRouter = require('routes/signup');
const loginRouter = require('routes/login');
const listRouter = require('routes/list');
const profileRouter = require('routes/profile');
const {SQLconnect} = require('db/utils');

SQLconnect()
var app = express();

app.set('strict routing', true);
app.use(jwt());
app.use(errorHandler);

app.use(logger('dev'));
app.use(bodyparser.json());
app.use(bodyparser.raw({
    limit: '10kb',
    type: 'application/json'
}));
app.use(bodyparser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/signup', signupRouter);
app.use('/login', loginRouter);
app.use('/me', profileRouter); //auth
app.use('/user', usersRouter);
app.use('/most-liked', listRouter);



module.exports = app;
