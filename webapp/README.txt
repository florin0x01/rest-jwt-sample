Sample web app for registering, logging in , liking, unliking, viewing profiles of users

Tech used: nodejs, express, MongoDb.

For MongoDb, two collections, one for users, one for userlikes. 
The most liked user is calculated using mongo aggregation pipeline.
The number of likes of a user is the document count for that user in the userlikes collection.


How to run ?

nvm use (if available). Or use node8
npm install - installs  dependencies.
npm test - runs test. Need to have global mocha installed

