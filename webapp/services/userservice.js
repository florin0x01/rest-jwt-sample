require('rootpath')();

const config = require('config');
const jwt = require('jsonwebtoken');
const {getRecordByUsernameAndPassword} = require('db/utils');

module.exports = {
    authenticate
}


async function signJwt(subject, secret, expire) {
    return new Promise((resolve, reject)=> {
        jwt.sign({ sub: subject }, secret, { expiresIn: expire }, (err,token)=> {
            if (err)
                return reject(err);
            return resolve(token);
        });
    });
}

async function authenticate(username, pass) {
    try {
        const user = await getRecordByUsernameAndPassword(username, pass);
        const token = await signJwt(user._id, config.secret,3600);
        return token;
    }catch(err) {
        console.log("Error", err)
        return null;
    }
}
