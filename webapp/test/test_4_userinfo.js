require('rootpath')();
require('test/setup');
const chai = require('chai');
const {register, getRequest} = require('test/common');
var assert = chai.assert;
const mongoose = require('mongoose');
const {getRecord} = require('db/utils');

describe ('User info', function() {
    it('Should list username and number of likes of a user', async function() {
        let username = 'testuserinfo' + Math.round((new Date()).getTime() / 1000); 
        await register(username);   
        let rec = await getRecord({username: username});
        let response = await getRequest('user/' + rec._id);
        assert.include(response.body, 'username');
        assert.include(response.body, 'likes');
    }); 

    it('Should fail to list username and number of likes of an inexistent user', async function() {
        let response = await getRequest('user/inexistent' + Math.round((new Date()).getTime() / 1000));
        assert.equal(response.body, '{"message":"Invalid id"}');
    }); 

    after(function() {
        // runs after all tests in this block
        console.log("Cleaning up");
       mongoose.connection.db.collection('users').remove({username: /^testuserinfo/});
    });
});    