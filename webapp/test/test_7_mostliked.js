require('rootpath')();
require('test/setup');
const chai = require('chai');
const {getRequest} = require('test/common');
var assert = chai.assert;
const mongoose = require('mongoose');
const {getRecord, getNumLikesforUser} = require('db/utils');

describe ('Most liked endpoint', function() {
    it('Should return most liked users in proper order, descending', async function() {
        let res =  await getRequest('most-liked');
        assert.equal(res.body,'[{"_id":"testmostlikedwalter","total":3},{"_id":"testmostlikedjohn","total":2},{"_id":"testmostlikedruby","total":1}]');        
    });

    before(function(){
        mongoose.connection.db.collection('userlikes').insertMany([{
            toName: "testmostlikedwalter",
            fromId: "0x1",
            toId: "0x2",
            "value": 1
        },
        {
            toName: "testmostlikedwalter",
            fromId: "0x3",
            toId: "0x2",
            "value": 1
        }, {
            toName: "testmostlikedwalter",
            fromId: "0x4",
            toId: "0x2",
            "value": 1
        }, {
              toName: "testmostlikedjohn",
              fromId: "0x2",
              toId: "0x9",
              "value": 1
            },

            {
                toName: "testmostlikedjohn",
                fromId: "0x8",
                toId: "0x9",
                "value": 1
            },

            {
                toName: "testmostlikedruby",
                fromId: "0x9",
                toId: "0x6",
                "value": 1
            }
        ], 
            
         (err, res)=> {

         });
    });

   
    after(function() {
        // runs after all tests in this block
        console.log("Cleaning up");
       mongoose.connection.db.collection('userlikes').remove({toName: /^testmostliked/});
    });
});    