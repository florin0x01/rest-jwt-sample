require('rootpath')();
require('test/setup');
const chai = require('chai');
const {register, login, extractJWT, postRequest} = require('test/common');
var assert = chai.assert;
const mongoose = require('mongoose');
const {getRecord, getNumLikesforUser} = require('db/utils');

describe ('Like user', function() {
    it('Should like a user only once', async function() {
        let username1 = 'testlikea' + Math.round((new Date()).getTime() / 1000); 
        let username2 = 'testlikeb' + Math.round((new Date()).getTime() / 1000); 
        await register(username1);   
        await register(username2);   
        let response0 = await login(username1);
        let token0=extractJWT(response0.body);   
        let response1 = await login(username2);
        let token1 = extractJWT(response1.body);

        let rec1 = await getRecord({username: username1});
        let rec2 = await getRecord({username: username2});

        let response2 = await postRequest('user/' + rec2._id + '/like', {},
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token0
            }
        )
        assert.equal(response2.statusCode,'200');

        let response3 = await postRequest('user/' + rec1._id + '/like', {},
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token1
            }
        )
        assert.equal(response3.statusCode,'200');

        let response4 = await postRequest('user/' + rec2._id + '/like', {},
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token0
            }
        )
        assert.equal(response4.statusCode, '400');

        let response5 = await postRequest('user/' + rec1._id + '/like', {},
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token1
            }
        )
        assert.equal(response5.statusCode, '400');

        let numLikes1 = await getNumLikesforUser(rec2._id);
        assert.equal(numLikes1, 1);

        let numLikes2 = await getNumLikesforUser(rec1._id);
        assert.equal(numLikes2, 1);
    }); 

    it('Should not like himself/herself', async function() {
        let username1 = 'testlikec' + Math.round((new Date()).getTime() / 1000); 
        await register(username1);   
        let response0 = await login(username1);
        let token0=extractJWT(response0.body);   
        let rec1 = await getRecord({username: username1});
        let response1 = await postRequest('user/' + rec1._id + '/like', {},
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token0
            }
        )   
        assert.equal(response1.statusCode, '400');
        assert.include(response1.body, 'myself');
    });

    it('Should not be able to like without token', async function() {
        let response1 = await postRequest('user/somebody1/like', {},
            {
                'Content-Type': 'application/json'
            }
        )   
        assert.equal(response1.statusCode, '401');
        assert.include(response1.body, 'Invalid');
    });

    it('Should not be able to like an inexistent user', async function() {
        let username1 = 'testliked' + Math.round((new Date()).getTime() / 1000); 
        await register(username1);   
        let response0 = await login(username1);
        let token0=extractJWT(response0.body);   
        let response1 = await postRequest('user/inexistentid1/like', {},
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token0
            }
        )   
        assert.equal(response1.statusCode, '400');
        assert.equal(response1.body, '{"message":"No username"}');
    });

   
    after(function() {
        // runs after all tests in this block
        console.log("Cleaning up");
       mongoose.connection.db.collection('users').remove({username: /^testlike/});
       mongoose.connection.db.collection('userlikes').remove({toName: /^testlike/});
    });
});    