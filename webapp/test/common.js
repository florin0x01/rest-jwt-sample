let request = require('async-request');
const url = 'http://localhost:3000';


async function postRequest(endpoint, dict, _headers = {'Content-Type': 'application/json'}, _method='POST') {
    return _postRequest(url + '/' + endpoint, dict, _headers, _method);
}

async function _postRequest(url, dict, _headers = {'Content-Type': 'application/json'}, _method='POST') {
    if (dict === undefined) dict = {};
    let response = await request(url, {
        method: _method,
        data: dict,
        headers: _headers
    });
    return response;
}

async function getRequest(endpoint, _headers = {'Content-Type': 'application/json'}) {
    let response = await request(url + '/' + endpoint, {
        method: 'GET',
        headers: _headers
    });
    return response;
}

async function register(username, password='pass1') {
    let response = await _postRequest(url + '/signup', 
         {
            'username': username,
            'password': password
         },
    );
     return response;
}

async function login(username, password='pass1') {
    let response = await _postRequest(url + '/login', 
         {
            'username': username,
            'password': password
         },
    );
    return response;
}

function extractJWT(msg) {
    let parsedmsg = JSON.parse(msg);
    return parsedmsg.status;
   // "status": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..baGDMGr4xrvKnzCS76RsqyaJs5esXy9qjSxcSk94Sc8"
}

module.exports = {
    register,
    login,
    postRequest,
    getRequest,
    extractJWT
}