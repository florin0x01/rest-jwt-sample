require('rootpath')();

require('test/setup');

const mongoose = require('mongoose');
const chai = require('chai');
var assert = chai.assert;
const {register} = require('test/common');


describe ('Signup', function() {
it('Should return success when signing up with new username and password', async function() {
    let username = 'testuser1' + Math.round((new Date()).getTime() / 1000); 
    let response = await register(username);   
    assert.equal(response.body, '{"signup":1}');  
    return;
});
it('Should fail when signing up with new username and password length < 4', async function() {
    let username = 'testuser5' + Math.round((new Date()).getTime() / 1000); 
    let response = await register(username,'pas');   
    assert.equal(response.statusCode, '400');  
    return;
});

it('Should fail when signing up with old username', async function() {
  let username = 'testuser2' + Math.round((new Date()).getTime() / 1000); 
  let response = await register(username);   
  assert.equal(response.body, '{"signup":1}');  
  let username2 = username;
  let expectedString = '{"message":"User validation failed: username: Error, expected `username` to be unique. Value: `' + username2 + '`"}'
  let response2 = await register(username2);
  assert.equal(response2.body, expectedString);
  return;
});

it('Should fail when using wrong username pattern', async function() {
    let username = 'TESTuser'
    let response = await register(username);   
    assert.include(response.body, 'User validation failed: username: Path `username` is invalid');  
})

it('Should fail when not supplying username', async function() {
    let username = ''
    let response = await register(username);   
    assert.equal(response.body, '{"message":"Invalid request"}');  
})

it('Should fail when not supplying password', async function() {
    let username = 'test123-' + Math.round((new Date()).getTime() / 1000); 
    let response = await register(username, '');   
    assert.equal(response.body, '{"message":"Invalid request"}');  
})


after(function() {
    // runs after all tests in this block
    console.log("Cleaning up");
   mongoose.connection.db.collection('users').remove({username: /^test/});
});

});

  
