require('rootpath')();
require('test/setup');
const chai = require('chai');
const {register, login} = require('test/common');
var assert = chai.assert;
const mongoose = require('mongoose');

describe ('Login', function() {
    it('Should return JWT token when logging in with current username and password', async function() {
        let username = 'testuserlogin' + Math.round((new Date()).getTime() / 1000); 
        await register(username);   
        let response = await login(username);
        assert.include(response.body, "status");
    });

    it('Should fail when logging in with invalid username', async function() {
        let response = await login('');
        assert.include(response.body, "message");
    });

    it('Should fail when logging in with invalid password and correct username', async function() {
        let username = 'testuserlogin2' + Math.round((new Date()).getTime() / 1000); 
        await register(username);   
        let response = await login(username, 'invalidpassword');
        assert.include(response.body, "message");
    });

    after(function() {
        // runs after all tests in this block
        console.log("Cleaning up");
       mongoose.connection.db.collection('users').remove({username: /^testuserlogin/});
    });
});    