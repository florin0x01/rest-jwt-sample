require('rootpath')();
require('test/setup');
const chai = require('chai');
const {register, postRequest, login, extractJWT, getRequest} = require('test/common');
var assert = chai.assert;
const mongoose = require('mongoose');


describe ('Logged in user operations', function() {
    it ('Should get the user info for a logged in user', async function() {
        let username = 'testuserme'+Math.round((new Date()).getTime() / 1000);
        await register(username, 'pass1');
        let respLogin = await login(username, 'pass1');
        let token=extractJWT(respLogin.body);       
        let response = await getRequest('me',
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        )
        assert.include(response.body, username);
    })

    it ('Should fail to get the user info for an incorrectly logged in user', async function() {
        let username = 'testuserme'+Math.round((new Date()).getTime() / 1000);
        let response = await getRequest('me',
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer 123'
            }
        )
        assert.include(response.body, 'Invalid Token');
    })

    it ('Should update the current logged in user password to a new one and login with the new password and get the profile', async function() {
        let username = 'testuserme3'+Math.round((new Date()).getTime() / 1000);
        await register(username, 'pass1');
        let respLogin = await login(username, 'pass1');
        let token=extractJWT(respLogin.body);       
        let response = await postRequest('me/update-password',
            {
                'password': 'password2'
            },
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        )
        assert.equal(response.body, '{"message":"ok"}');
        let respLogin2 = await login(username, 'password2');
        let token2 = extractJWT(respLogin2.body);
        let response2 = await getRequest('me',
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token2
            }
        )
        assert.include(response2.body, username);
    })

    after(function() {
        // runs after all tests in this block
        console.log("Cleaning up");
       mongoose.connection.db.collection('users').remove({username: /^testuserme/});
    });
});    