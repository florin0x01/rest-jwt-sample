require('rootpath')();
require('test/setup');
const chai = require('chai');
const {register, login, extractJWT, postRequest} = require('test/common');
var assert = chai.assert;
const mongoose = require('mongoose');
const {getRecord, getNumLikesforUser} = require('db/utils');

describe ('Unlike user', function() {
    it('Should unlike a user only once', async function() {
        let username1 = 'testunlikea' + Math.round((new Date()).getTime() / 1000); 
        let username2 = 'testunlikeb' + Math.round((new Date()).getTime() / 1000); 
        await register(username1);   
        await register(username2);   
        let response0 = await login(username1);
        let token0=extractJWT(response0.body);   
  
        let rec1 = await getRecord({username: username1});
        let rec2 = await getRecord({username: username2});

        await postRequest('user/' + rec2._id + '/like', {},
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token0
            }
        )
    
        let numLikes1 = await getNumLikesforUser(rec2._id);
        assert.equal(numLikes1, 1);

        await postRequest('user/' + rec2._id + '/unlike', {},
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token0
            },
            'DELETE'
        )

        let numLikes2 = await getNumLikesforUser(rec2._id);
        assert.equal(numLikes2, 0);

        await postRequest('user/' + rec2._id + '/unlike', {},
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token0
            },
            'DELETE'
        )

        let numLikes3 = await getNumLikesforUser(rec2._id);
        assert.equal(numLikes3, 0);
    }); 

    it('Should not unlike himself/herself', async function() {
        let username1 = 'testunlikec' + Math.round((new Date()).getTime() / 1000); 
        await register(username1);   
        let response0 = await login(username1);
        let token0=extractJWT(response0.body);   
        let rec1 = await getRecord({username: username1});
        let response1 = await postRequest('user/' + rec1._id + '/unlike', {},
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token0
            },
            'DELETE'
        )   
        assert.equal(response1.statusCode, '400');
        assert.include(response1.body, 'myself');
    });

    it('Should not be able to unlike without token', async function() {
        let response1 = await postRequest('user/somebody1/unlike', {},
            {
                'Content-Type': 'application/json'
            },
            'DELETE'
        )   
        assert.equal(response1.statusCode, '401');
        assert.include(response1.body, 'Invalid');
    });

    it('Endpoint returns 200 for inexistent user because it does not check for id due to not being necessary', async function() {
        let username1 = 'testunliked' + Math.round((new Date()).getTime() / 1000); 
        await register(username1);   
        let response0 = await login(username1);
        let token0=extractJWT(response0.body);   
        let response1 = await postRequest('user/inexistentid1/unlike', {},
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token0
            },
            'DELETE'
        )   
        assert.equal(response1.statusCode, '200');
    });

   
    after(function() {
        // runs after all tests in this block
        console.log("Cleaning up");
       mongoose.connection.db.collection('users').remove({username: /^testunlike/});
    });
});    