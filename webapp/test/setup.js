require('rootpath')();

let sqlMongoConProm = null;
const {SQLconnect} = require('db/utils');

before('before all befores', function(){

   if(sqlMongoConProm){
      return sqlMongoConProm;
   }
   return sqlMongoConProm = new Promise((res,rej) => {
        SQLconnect();
        return res(1);
   });
   
});