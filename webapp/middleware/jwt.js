require('rootpath')();

const expressJwt = require('express-jwt');
const config = require('config')
const userService = require('services/userservice')

module.exports = {
    jwt,
    authenticate
}

function jwt() {
    const { secret } = config;
    console.log("Jwt secret: ", secret)
    return expressJwt({ secret }).unless({
        path: [
            // routes that don't require authentication
            '/signup',
            '/login',
            // /\/user/i,
            new RegExp('^\/user\/[0-9a-z]+$', 'i'),
            '/most-liked'
        ]
    });
}

async function authenticate(req, res, next) {
    try {
        let tok = await userService.authenticate(req.body.username, req.body.password);
        if (!tok)
            return res.status(400).json({ message: 'Username or password is incorrect' })
        return res.json({status: tok});

    }catch(err) {
        console.log("Error")
        return res.status(400).json({message: err.message});
    }
}